import SwiftUI

struct ContentView : View {
    private let interactor: Interactor
    @ObjectBinding private var viewModel: ViewModel
    @State var guess: String = ""
    
    
    init(interactor: Interactor = InteractorImpl()) {
        self.interactor = interactor
        self.viewModel = interactor.presenter.viewModel
    }
    
    var body: some View {
        VStack {
            Text("Number Guessing Game")
            .font(.title)
            .padding(.bottom, 20)
            
            Text("Guess the number between 1 and 100")
            
            TextField("", text: $guess)
            .textFieldStyle(.roundedBorder)
            .multilineTextAlignment(.center)
            
            Button("Check your guess") {
                self.interactor.check(guess: self.$guess.value)
            }
            .padding()
            .background(Color.blue)
            .foregroundColor(.white)
            .cornerRadius(5)
            
            Text($viewModel.guessResponse.value)
        }
        .padding()
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
