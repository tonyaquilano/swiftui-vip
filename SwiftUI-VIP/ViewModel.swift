import SwiftUI
import Combine

class ViewModel: BindableObject {
    let didChange = PassthroughSubject<ViewModel, Never>()
    
    var guessResponse: String = " " {
        didSet {
            didChange.send(self)
        }
    }
}
