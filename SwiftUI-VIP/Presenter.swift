protocol Presenter {
    var viewModel: ViewModel { get set }
    
    func invalidGuess()
    func tooHigh()
    func tooLow()
    func correct()
}

class PresenterImpl: Presenter {
    var viewModel: ViewModel
    
    init(viewModel: ViewModel = ViewModel()) {
        self.viewModel = viewModel
    }
    
    func invalidGuess() {
        viewModel.guessResponse = "Bro, that's not even an integer"
    }
    
    func tooLow() {
        viewModel.guessResponse = "That guess is too low"
    }
    
    func tooHigh() {
        viewModel.guessResponse = "That guess is too high"
    }
    
    func correct() {
        viewModel.guessResponse = "That's correct!"
    }
}
