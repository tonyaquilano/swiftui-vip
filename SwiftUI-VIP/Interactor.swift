protocol Interactor {
    func check(guess: String)
    
    var presenter: Presenter { get }
}

class InteractorImpl: Interactor {
    let presenter: Presenter
    let value: Int
    
    init(presenter: Presenter = PresenterImpl()) {
        self.presenter = presenter
        self.value = Int.random(in: 1 ... 100)
    }
    
    func check(guess: String) {
        guard let intGuess = Int(guess) else {
            presenter.invalidGuess()
            return
        }
        
        if intGuess < value {
            presenter.tooLow()
        } else if intGuess > value {
            presenter.tooHigh()
        } else {
            presenter.correct()
        }
    }
}
